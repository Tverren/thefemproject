#ifndef ELEMENT
#define ELEMENT

#include <core/gmpoint>
#include <core/gmarraylx>
#include <trianglesystem/gmtrianglesystem>
#include "node.h"


class Element{

public:

    Element();
    Element(GMlib::Array<GMlib::TSEdge<float>*> edges,
            GMlib::TSVertex<float>* p0,
            GMlib::TSVertex<float>* p1,
            int i, int j);

    float getDiagonal();
    float getNonDiagonal();
    float getLoadVector();
    int getI();
    int getJ();


protected:


private:

    GMlib::Array<GMlib::TSEdge<float>*> _edges;
    GMlib::TSVertex<float>* _p0;
    GMlib::TSVertex<float>* _p1;
    int _i;
    int _j;

};
#endif // ELEMENT

