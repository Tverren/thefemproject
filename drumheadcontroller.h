#ifndef DRUMHEADCONTROLLER
#define DRUMHEADCONTROLLER

#include "element.h"

#include <core/gmpoint>
#include <core/gmarraylx>
#include <trianglesystem/gmtrianglesystem>


class DrumheadController: public GMlib::TriangleFacets<float>{

public:

    DrumheadController();
    DrumheadController(GMlib::ArrayLX<GMlib::TSVertex<float>> innerVertex);

    //DrumheadController(GMlib::ArrayLX<GMlib::TSVertex<float>> innerVertex) : GMlib::TriangleFacets<float>(innerVertex){};


    void initiate_drum();
    void boundary_check();
    void find_elements();
    void computing_stiffness_matrix();
    GMlib::TSEdge<float>* check_neighbor(GMlib::TSVertex<float>* vertex1, GMlib::TSVertex<float>* vertex2);


protected:

    void localSimulate(double dt){



        //varier mellom -+
        //endrer til noe bedre fordi ustabil

        //må fikse stiffness matrixen.
        _sumDT = _sumDT +(_oscillate ? dt:-dt);
        if(_sumDT >= 2 || _sumDT <= -2)
            _oscillate = !_oscillate;

        auto x = _invertedStiffnessMatrix *(_loadVector*_sumDT);

        for(int i = 0; i < _innerVertex.size(); i++){
            _innerVertex[i]->setZ(x[i]);
        }
        this->replot();
    }




private:
    GMlib::ArrayLX<GMlib::TSVertex<float>*> _innerVertex;
    GMlib::ArrayLX<Element> _arrayOfElements;
    GMlib::DMatrix<float> _stiffnessMatrix;
    GMlib::DMatrix<float> _invertedStiffnessMatrix;
    GMlib::DVector<float> _loadVector;
    float _sumDT;
    bool _oscillate;


};





#endif // DRUMHEADCONTROLLER

