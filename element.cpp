
#include "element.h"


Element::Element(){

}

Element::Element(GMlib::Array<GMlib::TSEdge<float>*> edges,
                 GMlib::TSVertex<float>* p0,
                 GMlib::TSVertex<float>* p1,
                 int i, int j){
    this->_edges = edges;
    this->_p0 = p0;
    this->_p1 = p1;
    this->_i = i;
    this->_j = j;
}


int Element::getI(){
    return _i;
}

int Element::getJ(){
    return _j;
}


float Element::getDiagonal(){
    GMlib::Vector<float,2> d1;
    GMlib::Vector<float,2> d2;
    GMlib::Vector<float,2> d3;

    float tk = 0.0f;
    float tk1 = 0.0f;
    float tk2 = 0.0f;

    auto triangles = _p0->getTriangles();

    for(int i = 0; i < triangles.size(); i++){

        auto triangleVertices = triangles[i]->getVertices();

        if(_p0 == triangles[i]->getVertices()[1]){
            std::swap(triangleVertices[0],triangleVertices[1]);
            std::swap(triangleVertices[1],triangleVertices[2]);
        }
        if(_p0 == triangles[i]->getVertices()[2]){
            std::swap(triangleVertices[0],triangleVertices[2]);
            std::swap(triangleVertices[1],triangleVertices[2]);
        }

        GMlib::TSVertex<float>* p1 = triangleVertices[1];
        GMlib::TSVertex<float>* p2 = triangleVertices[2];

        d1 = p2->getPosition() - _p0->getPosition();
        d2 = p1->getPosition() - _p0->getPosition();
        d3 = p2->getPosition() - p1->getPosition();

        tk1 = d3*d3;
        tk2 = 2*std::abs(d1^d2);
        tk += tk1/tk2;

        //tk += ((d3*d3)/2*std::abs(d1^d2));
    }
    return tk;
}

float Element::getNonDiagonal(){

    GMlib::Vector<float,2> d;
    GMlib::Vector<float,2> a1;
    GMlib::Vector<float,2> a2;

    auto triangle1 = _edges[0]->getTriangle()[0];
    auto triangle2 = _edges[0]->getTriangle()[1];

    GMlib::TSVertex<float>* p2;
    GMlib::TSVertex<float>* p3;

    for(int i = 0; i < triangle1->getVertices().getSize(); i++){
        if(triangle1->getVertices()[i] != _p0 && triangle1->getVertices()[i] !=_p1){
            p2 = triangle1->getVertices()[i];
        }
    }

    for(int i = 0; i < triangle2->getVertices().getSize(); i++){
        if(triangle2->getVertices()[i] != _p0 && triangle2->getVertices()[i] !=_p1){
            p3 = triangle2->getVertices()[i];
        }
    }

    d = _p1->getPosition() - _p0->getPosition();
    a1 = p2->getPosition() - _p0->getPosition();
    a2 = p3->getPosition() - _p0->getPosition();

    float dd = (1/(d*d));

    float area1;
    float dh1;
    float h1;

    area1 = std::abs(d^a1);
    dh1 = dd*(a1*d);
    h1 = dd*area1*area1;

    float area2;
    float dh2;
    float h2;

    area2 = std::abs(d^a2);
    dh2 = dd*(a2*d);
    h2 = dd*area2*area2;

    return ((dh1*(1-dh1)/h1)-dd)*(area1/2)+((dh2*(1-dh2)/h2)-dd)*(area2/2);
}

float Element::getLoadVector(){
    GMlib::Vector<float,2> d1;
    GMlib::Vector<float,2> d2;

    float tk = 0;

    auto triangles = _p0->getTriangles();

    for(int i = 0; i < triangles.size(); i++){

        auto triangleVertices = triangles[i]->getVertices();

        if(_p0 == triangleVertices[1]){
            std::swap(triangleVertices[0],triangleVertices[1]);
            std::swap(triangleVertices[1],triangleVertices[2]);
        }
        if(_p0 == triangleVertices[2]){
            std::swap(triangleVertices[0],triangleVertices[2]);
            std::swap(triangleVertices[1],triangleVertices[2]);
        }

        GMlib::TSVertex<float>* p1 = triangleVertices[1];
        GMlib::TSVertex<float>* p2 = triangleVertices[2];

        d1 = p2->getPosition() - _p0->getPosition();
        d2 = p1->getPosition() - _p0->getPosition();


        tk += std::abs(d1^d2)/6;
    }
    //std::cout << "tk " << tk << std::endl;
    return tk;
}










