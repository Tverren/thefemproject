#ifndef NODE
#define NODE


#include <core/gmpoint>
#include <core/gmarraylx>
#include <trianglesystem/gmtrianglesystem>

class Node : GMlib::TSVertex<float>{

public:

    Node();



    GMlib::TSVertex<float> vertexes();

protected:


private:

    GMlib::TSVertex<float> _vertexes;


};
#endif // NODE

