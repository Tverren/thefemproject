#include "drumheadcontroller.h"


DrumheadController::DrumheadController()
{

}

DrumheadController::DrumheadController(GMlib::ArrayLX<GMlib::TSVertex<float>> innerVertex) : GMlib::TriangleFacets<float>(innerVertex){
    _sumDT = 0;
}

void DrumheadController::initiate_drum()
{
    this->triangulateDelaunay();
    boundary_check();
    find_elements();
    computing_stiffness_matrix();
    this->computeNormals();
    this->replot();
}

void DrumheadController::boundary_check(){

    for(int i = 0; i < this->size(); i++){
        if(this->getVertex(i)->boundary() == false){
            _innerVertex += this->getVertex(i);
        }
    }
}


void DrumheadController::find_elements(){
    for(int i = 0; i < _innerVertex.size(); i++){
        for(int j = i; j < _innerVertex.size(); j++){

            GMlib::Array<GMlib::TSEdge<float>*> temporary_edge;
            if(i == j){
                temporary_edge = _innerVertex[i]->getEdges();
                _arrayOfElements += Element(temporary_edge,_innerVertex[i],_innerVertex[j],i,j);
            }
            else if(check_neighbor(_innerVertex[i],_innerVertex[j]) != nullptr){
                temporary_edge += check_neighbor(_innerVertex[i],_innerVertex[j]);
                _arrayOfElements += Element(temporary_edge,_innerVertex[i],_innerVertex[j],i,j);
            }
        }
    }
}


//checks if two neighboring vertices have the same edge
GMlib::TSEdge<float>* DrumheadController::check_neighbor(GMlib::TSVertex<float>* vertex1, GMlib::TSVertex<float>* vertex2){
    auto edges = vertex1->getEdges();

    for(int i = 0; i < edges.getSize(); i++){
        if(edges[i]->getOtherVertex(*vertex1) == vertex2)
            return edges[i];
    }
    return nullptr;

}


void DrumheadController::computing_stiffness_matrix(){

    _stiffnessMatrix.setDim(_innerVertex.getSize(),_innerVertex.getSize());
    _loadVector.setDim(_innerVertex.getSize());

    for(int i = 0; i < _stiffnessMatrix.getDim1(); i++){
        for(int j = 0; j < _stiffnessMatrix.getDim2(); j++){
            _stiffnessMatrix[i][j] = 0.0f;
        }
    }
    for(int i = 0; i < _arrayOfElements.size(); i++){
        auto current_element =_arrayOfElements[i];

        if(current_element.getI() != current_element.getJ()){
            _stiffnessMatrix[current_element.getI()][current_element.getJ()]
            =_stiffnessMatrix[current_element.getJ()][current_element.getI()]
            = current_element.getNonDiagonal();
        }
        else{
            _stiffnessMatrix[current_element.getI()][current_element.getJ()]
            = current_element.getDiagonal();

            _loadVector[current_element.getJ()] = current_element.getLoadVector();
            //_loadVector[current_element.getI()] = current_element.getLoadVector();
        }
    }
    /*
    std::cout << "load vector " << _loadVector << std::endl;
    std::cout << " " << std::endl;
    std::cout << "_stiffnessMatrix " << _stiffnessMatrix << std::endl;
    std::cout << " " << std::endl;
    std::cout << " _arrayOfElements " << _arrayOfElements.size() << std::endl;
    */

    _invertedStiffnessMatrix = _stiffnessMatrix.invert();
}





















